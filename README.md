# README #

This repository is for the collaborative work on iPIC3D Hackathon.

### How to compile on Titan ###

Setup PGI OpenACC environment on Titan:

```
$ module switch pgi pgi/14.7.0
$ module load cudatoolkit
$ module load cray-hdf5
$ module load cmake
```

Setup Cray OpenACC environment on Titan:
```
$ module switch PrgEnv-pgi PrgEnv-cray
$ module load craype-accel-nvidia35
$ module load cray-hdf5
$ module load cmake
```

To compile on Titan using either PGI or Cray
```
$ cd ipic3d-hackathon
$ mkdir build
$ cd build
$ cmake .. -DOPENACC=on
$ make
```
