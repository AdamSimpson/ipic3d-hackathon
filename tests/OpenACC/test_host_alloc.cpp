#include <iostream>
#include "assert.h"
#include "Alloc.h"
#include "stdio.h"

void test_1D()
{
    size_t dim = 100;

    // Allocate arrays
    char   *a1 = newArray1<char>(dim); 
    float  *a2 = newArray1<float>(dim);
    double *a3 = newArray1<double>(dim);

    // Fill with data on host
    for(int i=0; i<dim; i++) {
        a1[i] = 'h';
        a2[i] = 0x80000000;
        a3[i] = 0x1.921fb5p+1;
    }    

    // Assert host data
    for(int i=0; i<dim; i++) {
        assert(a1[i] == 'h');
        assert(a2[i] == 0x80000000);
        assert(a3[i] == 0x1.921fb5p+1);
    }

    // Free arrays
    delArray1(a1);
    delArray1(a2);
    delArray1(a3);

    std::cout<<"Passed test_1D"<<std::endl;
}

void test_2D()
{
    size_t dim1 = 100;
    size_t dim2 = 200;

    // Allocate arrays
    char   **a1 = newArray2<char>(dim1, dim2);
    float  **a2 = newArray2<float>(dim1, dim2);
    double **a3 = newArray2<double>(dim1, dim2);

    // Fill with data on host
    for(int i=0; i<dim1; i++) {
       for(int j=0; j<dim2; j++) {
            a1[i][j] = 'h';
            a2[i][j] = 0x80000000;
            a3[i][j] = 0x1.921fb5p+1;
        }
    }

    // Assert host data
    for(int i=0; i<dim1; i++) {
        for(int j=0; j<dim2; j++) {
            assert(a1[i][j] == 'h');
            assert(a2[i][j] == 0x80000000);
            assert(a3[i][j] == 0x1.921fb5p+1);
        }
    }

    // Free arrays
    delArray2(a1);
    delArray2(a2);
    delArray2(a3);

    std::cout<<"Passed test_2D"<<std::endl;
}

void test_3D()
{
    size_t dim1 = 100;
    size_t dim2 = 200;
    size_t dim3 = 300;

    // Allocate arrays
    char   ***a1 = newArray3<char>(dim1, dim2, dim3);
    float  ***a2 = newArray3<float>(dim1, dim2, dim3);
    double ***a3 = newArray3<double>(dim1, dim2, dim3);

    std::cout<<"I wish I could print this\n\n";

    // Fill with data on host
    for(int i=0; i<dim1; i++) {
       for(int j=0; j<dim2; j++) {
           for(int k=0; k<dim3; k++) {
                a1[i][j][k] = 'h';
                a2[i][j][k] = 0x80000000;
                a3[i][j][k] = 0x1.921fb5p+1;
            }
        }
    }

    // Assert host data
    for(int i=0; i<dim1; i++) {
        for(int j=0; j<dim2; j++) {
            for(int k=0; k<dim3; k++) {
                assert(a1[i][j][k] == 'h');
                assert(a2[i][j][k] == 0x80000000);
                assert(a3[i][j][k] == 0x1.921fb5p+1);
            }
        }
    }

    // Free arrays
    delArray3(a1);
    delArray3(a2);
    delArray3(a3);

    std::cout<<"Passed test_3D"<<std::endl;
}

void test_4D()
{
    size_t dim1 = 20;
    size_t dim2 = 200;
    size_t dim3 = 300;
    size_t dim4 = 400;

    // Allocate arrays
    char   ****a1 = newArray4<char>(dim1, dim2, dim3, dim4);
    float  ****a2 = newArray4<float>(dim1, dim2, dim3, dim4);
    double ****a3 = newArray4<double>(dim1, dim2, dim3, dim4);

    // Fill with data on host
    for(int i=0; i<dim1; i++) {
       for(int j=0; j<dim2; j++) {
           for(int k=0; k<dim3; k++) {
               for(int t=0; t<dim4; t++) {
                    a1[i][j][k][t] = 'h';
                    a2[i][j][k][t] = 0x80000000;
                    a3[i][j][k][t] = 0x1.921fb5p+1;
                }
            }
        }
    }

    // Assert host data
    for(int i=0; i<dim1; i++) {
        for(int j=0; j<dim2; j++) {
            for(int k=0; k<dim3; k++) {
                for(int t=0; t<dim4; t++) {
                    assert(a1[i][j][k][t] == 'h');
                    assert(a2[i][j][k][t] == 0x80000000);
                    assert(a3[i][j][k][t] == 0x1.921fb5p+1);
                }
            }
        }
    }

    // Free arrays
    delArray4(a1);
    delArray4(a2);
    delArray4(a3);

    std::cout<<"Passed test_4D"<<std::endl;
}

int main(int argc, char *argv[])
{
    test_1D();
    test_2D();
    test_3D();
    test_4D();

    return 0;
}
